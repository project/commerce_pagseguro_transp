CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module integrates PagSeguro Transparente, brazilian payment provider, into
the Drupal Commerce payment and checkout systems.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/commerce_pagseguro_transp

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/commerce_pagseguro_transp

REQUIREMENTS
------------

This module requires the following modules:

 * commerce (https://www.drupal.org/project/commerce)
 * cpf (https://drupal.org/project/cpf)

INSTALLATION
------------

We recommend use Composer to download this module and its dependencies.
You can use the command: composer require drupal/commerce_pagseguro_transp
on the root of your Drupal project. To more information:
https://www.drupal.org/docs/8/modules/installing-commerce-pagseguro-transparente

CONFIGURATION
-------------

After installation go to Commerce -> Config -> Payment -> Payment gateway
(/admin/commerce/config/payment-gateways) and fill the required fields.

MAINTAINERS
-----------

Current maintainers:

 * Bruno de Oliveira Magalhaes (bmagalhaes) - https://drupal.org/user/333078
 * Ronan Ribeiro (RonanRBR) - https://drupal.org/user/332925

This project has been sponsored by:

 * 7Links Web Solutions
   Specialized in consulting and planning of Drupal powered sites, 7Links
   offers installation, development, theming, customization, and hosting
   to get you started. Visit https://7links.com.br for more information.
