<?php

namespace Drupal\commerce_pagseguro_transp\EventSubscriber;

use CommerceGuys\Addressing\AddressFormat\AdministrativeAreaType;
use Drupal\address\Event\AddressEvents;
use Drupal\address\Event\AddressFormatEvent;
use Drupal\address\Event\SubdivisionsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddressEventSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    $events[AddressEvents::ADDRESS_FORMAT][] = ['onGetDefinition', 0];

    return $events;
  }

  public function onGetDefinition($event) {
    $definition = $event->getDefinition();

    // Remove family name form fields
    if (($key = array_search('familyName', $definition['required_fields'])) !== false) {
      unset($definition['required_fields'][$key]);
    }

    $definition['required_fields'][] = 'dependentLocality';
    $definition['available_countries'] = ['BR'];
    $definition['address_line_2_title'] = 'Complemento';
    $definition['format'] = "%givenName\n%addressLine1\n%addressLine2\n%dependentLocality \n%administrativeArea %locality %postalCode";
    $event->setDefinition($definition);
  }

}
