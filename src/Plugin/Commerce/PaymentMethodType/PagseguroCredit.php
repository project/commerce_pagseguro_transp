<?php

namespace Drupal\commerce_pagseguro_transp\Plugin\Commerce\PaymentMethodType;

// Unused statments.
// Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase.
// Drupal\commerce_payment\Entity\PaymentMethodInterface.
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\CreditCard;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the Authorize.net eCheck payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "pagseguro_credit",
 *   label = @Translation("Credit Card"),
 *   create_label = @Translation("Credit Card"),
 * )
 */
class PagseguroCredit extends CreditCard {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['card_holder_name'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Holder name'))
      ->setDescription($this->t('The name of card holder'))
      ->setRequired(TRUE);

    $fields['installment_amount'] = BundleFieldDefinition::create('decimal')
      ->setLabel($this->t('Installment amount'))
      ->setSetting('decimal', '10.2')
      ->setDescription($this->t('The installment amount'))
      ->setRequired(TRUE);

    $fields['installments_qty'] = BundleFieldDefinition::create('integer')
      ->setLabel($this->t('Installments quantity'))
      ->setDescription($this->t('The installments quantity'))
      ->setRequired(TRUE);

    $fields['sender_hash'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Sender hash'))
      ->setDescription($this->t('The sender hash code returned by Pagseguro'))
      ->setRequired(TRUE);

    return $fields;
  }

}
